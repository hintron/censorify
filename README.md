This repo was created in going through the book "Node.js, MongoDB, and AngularJS Web Development".

It turned out my file did not match my "main" property in package.json. This was causing 'module not found' errors. I inadvertently named the file 'censorify.js' instead of 'censortext.js'.

Links that helped me figure this out:
https://quickleft.com/blog/creating-and-publishing-a-node-js-module/
https://nodejs.org/api/modules.html#modules_loading_from_node_modules_folders

The quickleft tutorial showed how the "main" property needs to point to the file in question.

Link:
http://proquest.safaribooksonline.com/book/programming/javascript/9780133844351